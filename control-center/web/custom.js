const MQTT_BROKER = "wss://spacebarbroker.current.at:9001"
console.log("MQTT_BROKER", MQTT_BROKER)

const toggles = {
    btnSonne: 'cmnd/sonoff_sonne/Power',
    btnDeckeBunt: 'cmnd/sonoff_decke_bunt/Power',
    btnZeppelin: 'cmnd/sonoff_zeppelin/Power',
    btnVentilator: 'cmnd/sonoff_ventilator/Power',
    btnAmbientFenster: 'cmnd/sonoff_ambientfenster/Power',
    btnFloDesklight: 'cmnd/sonoff_flo_desklight/Power',
    btnChrisHeadlight: 'cmnd/sonoff_chris_headlight/Power',
    btnFloKunst: 'cmnd/sonoff_kunst/Power',
    btnHackerstacker: 'cmnd/sonoff_hackerstacker/Power',
}


var client = mqtt.connect(MQTT_BROKER) // you add a ws:// url here

client.on('connect', () => {
    console.log('connected')
    $("#alertConnecting").hide()
    $("#alertError").hide()
    // $("#alertConnected").show()
})

client.stream.on('error', () => {
    console.log('error')
    $("#alertConnecting").hide()
    $("#alertConnected").hide()
    $("#alertError").show()
    // $("#alertConnecting").hide()
    // $("#alertConnected").show()
})

// Setup button click handlers, which toggle lights
for (const toggle in toggles) {
    $("#" + toggle).click(() => {
        console.log('mqtt send TOGGLE to ', toggles[toggle])
        client.publish(toggles[toggle], "TOGGLE")
    })
}


$("#btnAllOn").click(() => {
    for (const toggle in toggles) {
        if (toggle === 'btnVentilator') continue;
        client.publish(toggles[toggle], "ON")
    }
})

$("#btnAllOff").click(() => {
    for (const toggle in toggles) {
        client.publish(toggles[toggle], "OFF")
    }
})
