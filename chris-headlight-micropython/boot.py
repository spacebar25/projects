import network
import webrepl

SSID = "spacebar.ofBandits"
PASSWORD = "ZuluBravoHotel"

def do_connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(SSID, PASSWORD)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())

do_connect()

webrepl.start()

import machine, neopixel

np = neopixel.NeoPixel(machine.Pin(23), 320)
# np = neopixel.NeoPixel(machine.Pin(36), 10)

for i in range(np.n):
    np[i] = (255, 0, 0)

np.write()
