const MQTT_BROKER = "ws://10.4.0.63:9001"
console.log("MQTT_BROKER", MQTT_BROKER)


var effect_running=0;

var EFFECT_KITT=1;

var client = mqtt.connect(MQTT_BROKER) // you add a ws:// url here

client.on('connect', () => {
    console.log('connected')
    $("#alertConnecting").hide()
    $("#alertError").hide()
    // $("#alertConnected").show()
})

client.stream.on('error', () => {
    console.log('error')
    $("#alertConnecting").hide()
    $("#alertConnected").hide()
    $("#alertError").show()
    // $("#alertConnecting").hide()
    // $("#alertConnected").show()
})


/*
// Setup button click handlers, which toggle lights
for (const toggle in toggles) {
    $("#" + toggle).click(() => {
        console.log('mqtt send TOGGLE to ', toggles[toggle])
        client.publish(toggles[toggle], "TOGGLE")
    })
}
*/

$("#btnAllOn").click(() => {
        client.publish("HOB/windows/rows/4/rgb","128 0 64 0")
})


$("#btnAllOff").click(() => {
    client.publish("HOB/windows/rows/4/rgb","0 0 0 0")

})



//first line
$("#btnR").click(() => {
    client.publish("HOB/windows/rows/4/rgb","255 0 0 0")

})
$("#btnG").click(() => {
    client.publish("HOB/windows/rows/4/rgb","0 255 0 0")

})
$("#btnB").click(() => {
    client.publish("HOB/windows/rows/4/rgb","0 0 255 0")

})
$("#btnW").click(() => {
    client.publish("HOB/windows/rows/4/rgb","0 0 0 255")
})


// second line

$("#btnGelb").click(() => {
    client.publish("HOB/windows/rows/4/rgb","255 255 0 0")

})
$("#btnViolett").click(() => {
    client.publish("HOB/windows/rows/4/rgb","174 0 255 0")

})
$("#btnPink").click(() => {
    client.publish("HOB/windows/rows/4/rgb","128 0 64 0")

})
$("#btnMint").click(() => {
    client.publish("HOB/windows/rows/4/rgb","0 255 255 0") 

})

// third line



$("#btnRegenbogen").click(() => {

    effect_running=0;

    client.publish("HOB/windows/columns/1/rgb","255 0 0 0");
    client.publish("HOB/windows/columns/2/rgb","255 138 0 0");
    client.publish("HOB/windows/columns/3/rgb","255 245 0 0");
    client.publish("HOB/windows/columns/4/rgb","150 255 0 0");
    client.publish("HOB/windows/columns/5/rgb","0 255 33 0");
    client.publish("HOB/windows/columns/6/rgb","0 255 133 0");
    client.publish("HOB/windows/columns/7/rgb","0 217 255 0");
    client.publish("HOB/windows/columns/8/rgb","0 100 255 0");
    client.publish("HOB/windows/columns/9/rgb","0 0 255 0");
    client.publish("HOB/windows/columns/10/rgb","90 0 255 0");
    client.publish("HOB/windows/columns/11/rgb","255 0 200 0");
    client.publish("HOB/windows/columns/12/rgb","255 0 64 0");



})



var colors = ["red", "orange", "yellow", "green", "blue", "indigo", "violet"];

var i = 1;

window.setInterval(function(){
    $("#btnRegenbogen").css('background-color',colors[i]) ;
    i++;
    if (i === colors.length){
        i=0;
    }
}, 2000);
















$("#btnKitt").click(() => {
    if(effect_running==0)
    {
     //only if effect starts first time

     effect_running=EFFECT_KITT
     run_effects();
    }
    else
    {
     //clicked while running, turn effect off.
     effect_running=0;
     //run_effect()
    }
    //client.publish("HOB/windows/rows/4/rgb","255 255 0 0");


})






function run_effects()
{
    console.log("run_effects() called")
    if(effect_running)
    {
        //do the effect things
        if(effect_running==EFFECT_KITT)
        {
            kitt();
        }
        setTimeout(function(){ run_effects(); } , 300);
    }  
    else
    {
        // set aus
        client.publish("HOB/windows/rows/4/rgb","0 0 0 0");
    }

    
}



var kittcounter=0;
var kitdir=1;

function kitt()
{
console.log("kitt() " + kittcounter)
if(kitdir)
{
    kittcounter++;
    var pos =  (kittcounter%13)
    if(!pos) kitdir=0
}
else 
{
    kittcounter--;
    var pos =  (kittcounter%13)
    if(!pos) kitdir=1

}

    client.publish("HOB/windows/rows/4/rgb","64 0 0 0");
    setTimeout(function(){ client.publish("HOB/windows/columns/"+pos+"/rgb","255 0 0 0"); } , 50);
    

}
