Goal: Simple web interface with power toggles, sending mqtt messages

Live at:
* https://spacebar25.gitlab.io/projects/
* https://spacebar25.gitlab.io/projects/embedded.html

---

Uses [bootstrap buttons](https://getbootstrap.com/docs/4.0/components/buttons/), and [mqtt.js](https://github.com/mqttjs/MQTT.js) to send MQTT messages.

When new code is merged into master, a Gitlab [CI job](https://gitlab.com/spacebar25/projects/-/jobs) deploys the new web interface to https://spacebar25.gitlab.io/projects

---

Debug:

    mosquitto_sub -h 10.12.50.100 -v -t "#"
    mosquitto_pub -h 10.12.50.100 -m "TOGGLE" -t "cmnd/sonoff_chris_headlight/Power"

On the MQTT broker, enable MQTT and websocket in mosquitto:

`/etc/mosquitto/mosquitto.conf`

    listener 1883
    protocol mqtt

    listener 9001
    protocol websockets
