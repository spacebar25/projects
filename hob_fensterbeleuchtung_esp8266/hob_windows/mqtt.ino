////////////// MQTT
//#define MQTT_VERSION  3
#define MQTT_VERSION  MQTT_VERSION_3_1

#include <PubSubClient.h>





WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;







void mqtt_setup()
{
  Serial.println("mqtt_setup()");
  Serial.print("Setting up server ");
  Serial.print(mqtt_server);
  Serial.print(":");   
  Serial.println(atoi(mqtt_port));
        
 // client.setServer(mqtt_server, atoi(mqtt_port));
  // TODO FIX, da stimmt was nicht. das mqtt_port wird nicht im JSON gespeichert vom wifimanager :(
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback); 
  
}



void mqtt_publish(char* topic,char* message)
{



  char path[100]; 
  sprintf (path, "HOB/windows/clients/%s/%s", device_name , topic);

   Serial.print("PUBLISHING mqtt_publish('");
   Serial.print(path);
   Serial.print("','");
   Serial.print(message);
   Serial.println("')");

  client.publish(path, message);




}




void mqtt_publish_retained(char* topic,char* message)
{



  char path[100]; 
  sprintf (path, "HOB/windows/clients/%s/%s", device_name , topic);

   Serial.print("PUBLISHING RETAINED mqtt_publish('");
   Serial.print(path);
   Serial.print("','");
   Serial.print(message);
   Serial.println("')");

  client.publish(path, message, true);




}










/*
 *  This is called every time something is RECEIVED from the mqtt broker
 */


//String current_message ="";


char current_message[100];




void callback(char* topic, byte* payload, unsigned int length) 
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.println("] ");



  //current_message='';

  for(int i=0;i<length;i++) current_message[i] = payload[i];

  //strncpy(current_message, payload,length);
  current_message[length] = '\0';

  if(strstr(topic,"/effect"))
  {
   Serial.println("FOUND /effect via strstr()"); 
   led_handle_request_effect(topic, current_message);
   return;    
  }


  if(strstr(topic,"/rgb"))
  {
   Serial.println("FOUND /rgb via strstr()"); 
   led_handle_request_rgb(current_message);
   return;    
  }



}










void mqtt_connect() {
  
  // Loop until we're reconnected
   while (!client.connected()) 
   {
 
    Serial.print("Attempting MQTT connection...");
button_work();

    /*
    boolean connect (clientID, willTopic, willQoS, willRetain, willMessage)
    
    Connects the client with a Will message specified.
    
    Parameters
    clientID : the client ID to use when connecting to the server.
    willTopic : the topic to be used by the will message (const char[])
    willQoS : the quality of service to be used by the will message (int : 0,1 or 2)
    willRetain : whether the will should be published with the retain flag (boolean)
    willMessage : the payload of the will message (const char[])
    Returns
    false - connection failed.
    true - connection succeeded. 
     */

    char willTopic[] = "";

    //what is this?
    int willQos=1;
    
    // den disconnect status NICHT retainen!! sonst blasen wir die DB auf.
    int willRetain=1;    


    char topic[50]; 
    sprintf (topic, "HOB/windows/clients/%s/status", device_name);



    
    if (client.connect(device_name,topic,willQos,willRetain,"DISCONNECTED")) 
    {
      

      
      Serial.println("connected");
      // Once connected, publish an announcement...
//      mqtt_publish("status","CONNECTED");

      mqtt_publish_retained("status","CONNECTED");

    char rows_topic[50]; 
    sprintf (rows_topic, "HOB/windows/rows/%s/#", row);

    client.subscribe(rows_topic);

    char cols_topic[50]; 
    sprintf (rows_topic, "HOB/windows/columns/%s/#", column);

    client.subscribe(rows_topic);


    // global events

    client.subscribe("HOB/windows/controls/#");
    //controls/all/rgb
        

    // ... and subscribe to device specific events
    char device_subscriptions[50]; 
    sprintf (device_subscriptions, "HOB/windows/clients/%s/#", device_name); 
    client.subscribe(device_subscriptions);

    return;
  
      
      
      
      
    } 
    else 
    {
      // TODO ERROR INDICATION WITH SOUND/LIGHT

      //beep_fail();
      
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 1 second");
      delay(1000);


    }// if/else
  }// while (! connected)
}







void mqtt_work() // called from loop() and b_delay()
{



  if (!client.connected()) {
     mqtt_connect();
  }
  client.loop();

}
