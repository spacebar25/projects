//needs complete rewrite


#include <Adafruit_NeoPixel.h>

#include <stdio.h>
#include <string.h>


#define NUM_LEDS 70

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, 5, NEO_RGBW + NEO_KHZ800);











Ticker ledticker;



int effect_running=0;
int effect_parameter = 0;


int r=0;
int g=0; 
int b=0;
int w=0;

 int wait=0;
  
void test_leds()
{
set_all(255,0,0,0);
delay(500);
set_all(0,255,0,0);
delay(500);
set_all(0,0,255,0);
delay(500);
set_all(0,0,0,255);
delay(500);
set_all(0,0,0,0);

}

void leds_setup()
{
  
  
  Serial.println("leds_setup() ");



   strip.begin();
  strip.setBrightness(255);
  strip.show(); // Initialize all pixels to 'off'

  //rainbowCycle(20);


  pinMode(LED_BUILTIN,OUTPUT);
}


// set from timer callback function
int ticker_called = 0;

// called from callback
void leds_ticker()
{
 ticker_called=1;
}













void leds_all_off()
{
 r,g,b,w =0;
 set_all(0,0,0,0); 
}


void set_all(int r, int g, int b, int w)
{
 for(int i=0; i<strip.numPixels(); i++) {

  strip.setPixelColor(i,g,r,b,w);
 }
strip.show();

}



 



void led_handle_request_rgb(char* message)
{
 Serial.println("RGB ACTION - led_handle_request_rgb()");
 Serial.println(message);

 //static color!
 effect_running=3;

 char separator[] = " ";
 char *token;

  
 token = strtok(message, separator);
 int count=1;

   while(token != NULL)
   {
      if(count==1) r= atoi(token);
      if(count==2) g= atoi(token);
      if(count==3) b= atoi(token);
      if(count==4) w= atoi(token);       
       
 
      token = strtok(NULL, separator);
      count++;
   }
  
  
}

void chaser() {
  CylonBounce(0xff, 0, 0, 4, 10, 50);
}

void CylonBounce(byte red, byte green, byte blue, int EyeSize, int SpeedDelay, int ReturnDelay){

  for(int i = 0; i < NUM_LEDS-EyeSize-2; i++) {
    set_all(0,0,0,0);
    strip.setPixelColor(i, red/10, green/10, blue/10);
    for(int j = 1; j <= EyeSize; j++) {
      strip.setPixelColor(i+j, red, green, blue);
    }
    strip.setPixelColor(i+EyeSize+1, red/10, green/10, blue/10);
    strip.show();
    delay(SpeedDelay);
  }

  delay(ReturnDelay);

  for(int i = NUM_LEDS-EyeSize-2; i > 0; i--) {
    set_all(0,0,0,0);
    strip.setPixelColor(i, red/10, green/10, blue/10);
    for(int j = 1; j <= EyeSize; j++) {
      strip.setPixelColor(i+j, red, green, blue);
    }
    strip.setPixelColor(i+EyeSize+1, red/10, green/10, blue/10);
    strip.show();
    delay(SpeedDelay);
  }
 
  delay(ReturnDelay);
}

void led_handle_request_effect(char* topic, char* message)
{
 Serial.println("EFFECT ACTION - led_handle_request_effect()");

 char *token;

 r=0;
 g=0; 
 b=0;
 w=0;
  

 int count=1;
 int effectnumber= 0;


 char separator[] = "/";
 token = strtok(topic, separator);
 while(token != NULL)
 {
  effectnumber=atoi(token);
  token = strtok(NULL, separator);
 }

 token = strtok(message, separator);
 while(token != NULL)
 {
     wait=atoi(token);
     token = strtok(NULL, separator);
 }


 Serial.print("effect ");
 Serial.print(effectnumber);
 Serial.print(" wait: ");
 Serial.println(wait);

  effect_running=effectnumber;

  
}






















void leds_work()
{

   switch(effect_running)
   {

    
    
    case 0:
       return;
    break;

    
    case 1:
      rainbow();
    break;
    case 2:  
      rainbowFull();
    break;  

    case 3:
      set_all(r,g,b,w);
      effect_running=0;
    break;

    case 4:  
      chaser();
    break;  



    // falls wir den effekt nicht kennen drehen wir den aktuellen ab.
    default:
      effect_running=0;
      leds_all_off();
      
    return;
    }
}




//---------- RAINBOW EFFECT STUFF



void rainbow() {




  while(effect_running)
  {
  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through strip.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents

    if(b_delay(wait)) return;  // Pause for a moment
    if(effect_running !=1) return;
  }
  }
}



void rainbowFull() {
  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:

  // effect will only end if a new mwtt message is received (in b_delay)
 // while(effect_running)
 // {
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {

          int pixelHue = firstPixelHue + (65536L / strip.numPixels());
          
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
     
     

      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through strip.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents

    if(b_delay(wait)) return;  // Pause for a moment
    if(effect_running !=2) return;
  }
 // }
}

uint32_t last_mqtt_break=millis();

int b_delay(int wait)
{
   for(int i=0;i<wait;i+=5)
   {
    delay(5);
    if((millis()- last_mqtt_break) >1000)
    {
     last_mqtt_break=millis(); 
     //Serial.println("U");
     mqtt_work();
    }
   }
   return 0;
}
