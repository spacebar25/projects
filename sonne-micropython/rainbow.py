# CircuitPython demo - NeoPixel
import time

import machine, neopixel

num_pixels = 288
# pixels = neopixel.NeoPixel(machine.Pin(23), num_pixels, bpp=4)

pixels = neopixel.NeoPixel(machine.Pin(23), num_pixels, bpp=4)


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b, 0)


def make_rainbow():
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(pixel_index & 255)
        pixels.write()
        time.sleep(0.001)

def start():
    while True:
        make_rainbow()
