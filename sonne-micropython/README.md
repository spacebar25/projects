MicroPython code for rainbow in der Sonne.

* Board: [AZ Delivery ESP32-WROOM-32 NodeMCU](https://gitlab.com/spacebar25/private/-/wikis/home#micropython_sonne)
* WebREPL: http://micropython.org/webrepl/?#10.12.50.110:8266 (pwd: test)

# credentials.py

Wifi config is stored in `credentials.py` but not added to git. Create it
with this content:

```
SSID = "YOUR_WIFI_SSID"
PASSWORD = "WIFI_PASSWORD"
```

# Upload and run code

```
export WEBREPL_HOST=10.12.50.110
export WEBREPL_PASSWORD=test

webreplcmd ls
webreplcmd cat boot.py

webreplcmd put credentials.py credentials.py
webreplcmd put boot.py boot.py
webreplcmd put rainbow.py rainbow.py
webreplcmd put main.py main.py
```

Soft reset the device (eg. in WebREPL, press Ctrl+D)
