################
# Previous tests
################
import machine, neopixel
import time

np = neopixel.NeoPixel(machine.Pin(23), 288, bpp=4)

def write_color_to_all(r, g, b, w=0):
     for i in range(np.n):
         np[i] = (r, g, b, w)
     np.write()


def demo1():
     # cycle
     for i in range(4 * np.n):
         for j in range(np.n):
             np[j] = (0, 0, 0, 0)
         np[i % n] = (255, 255, 255, 0)
         np.write()
         time.sleep_ms(25)

write_color_to_all(100, 50, 0, 00)

