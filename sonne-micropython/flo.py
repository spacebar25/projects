import machine 
import neopixel
import time


#how many pixels are connected?
num_pixels = 288

# segment definitions
#segment_stripes=(66,67,69,71,8,9,10,6,63)  #USED


segment_ring0=(23,24, 71,72, 119,120, 167,168, 215,216,  263,264) #USED



pixels = neopixel.NeoPixel(machine.Pin(23), num_pixels, bpp=4)

displaybuffer=[None]*num_pixels
stepcounter=0





# animatio superclass
class Animation:
    def __init__(self, segment, color):
        self.segment = segment
        self.color = color
        self.callcounter = 0
        self.updatecounter = 0
        self.updateinterval = 1
        self.direction = 0
        self.max_pixel=len(self.segment)
        self.effect_offset=0
        self.runstate=0



class Glow(Animation):
    def run(self):
        self.callcounter+=1
        if( (self.callcounter%self.updateinterval) == 0 ):
            self.updatecounter+=1

    
        if((self.updatecounter%255) ==0 ):
            if(self.runstate): 
                self.runstate=0
            else: 
                self.runstate=1    
        

        for i in self.segment:
            if(self.runstate):
                displaybuffer[i]=(0,(self.updatecounter%255),0) 
            else:
                displaybuffer[i]=(0,255-(self.updatecounter%255),0)   
 



# scanner animation implementation
class Scanner(Animation):
    def run(self):
        self.callcounter+=1
        if( (self.callcounter%self.updateinterval) == 0 ):
            self.updatecounter+=1

        
        currentpixel = self.updatecounter % self.max_pixel


        if(self.direction):
            displaybuffer[self.segment[currentpixel]]=self.color 
        else:
            displaybuffer[self.segment[self.max_pixel-currentpixel-1]]=self.color 


# set full segment to specific color
class FullSegment(Animation):
    def run(self):
        for i in self.segment:
            displaybuffer[i]=self.color 








# rainbow superclass
class RainbowAnimation(Animation):
    def wheel(self,pos):
        # Input a value 0 to 255 to get a color value.
        # The colours are a transition r - g - b - back to r.
        if pos < 0 or pos > 255:
            return (0, 0, 0)
        if pos < 85:
            return (255 - pos * 3, pos * 3, 0,0)
        if pos < 170:
            pos -= 85
            return (0, 255 - pos * 3, pos * 3,0)
        pos -= 170
        return (pos * 3, 0, 255 - pos * 3,0)

# concrete rainbow implementation
class RainbowCycle(RainbowAnimation):
    def run(self):
        self.callcounter+=1
        if( (self.callcounter%self.updateinterval) == 0 ):
            self.updatecounter+=1


        for i in range(self.max_pixel):
            rc_index = (i * 256 // self.max_pixel) + (self.updatecounter*self.effect_offset)  
            displaybuffer[self.segment[i]] = self.wheel((rc_index & 255))



# another concrete rainbow implementation
class RainbowScanner(RainbowAnimation):
    def run(self):
        self.callcounter+=1
        if( (self.callcounter%self.updateinterval) == 0 ):
            self.updatecounter+=1

        for i in range(self.max_pixel):
            displaybuffer[self.segment[i]]=(0,0,0,0)
            
        
        rc_index = (i * 256 // self.max_pixel) + (self.updatecounter*self.effect_offset)  
        #if(self.direction):
        #displaybuffer[self.segment[self.updatecounter%self.max_pixel]] = self.wheel((rc_index & 255))
        #else:
        displaybuffer[self.segment[(self.max_pixel-self.updatecounter)%self.max_pixel]] = self.wheel((rc_index & 255))
            
        


# concrete rainbow implementation
class RainbowFullSegment(RainbowAnimation):
    def run(self):
        self.callcounter+=1
        if( (self.callcounter%self.updateinterval) == 0 ):
            self.updatecounter+=1


        for i in range(self.max_pixel):
            displaybuffer[self.segment[i]] = self.wheel((self.updatecounter & 255))

# concrete rainbow implementation
class RainbowRings(RainbowAnimation):
    def run(self):
        self.callcounter+=1
        if( (self.callcounter%self.updateinterval) == 0 ):
            self.updatecounter+=1



        for start in self.segment:
            if ( start % 2 ):
                for i in range(24):
                    displaybuffer[(start - i)]=self.wheel(((self.updatecounter + i*4) & 255))
            else:
                for i in range(24):
                    displaybuffer[(start + i)]=self.wheel(((self.updatecounter + i*4) & 255))
                
# concrete rainbow implementation
class Regenbogensemmel(RainbowAnimation):
    def run(self):
        self.callcounter+=1
        if( (self.callcounter%self.updateinterval) == 0 ):
            self.updatecounter+=1



        for start in self.segment:
            if ( start % 2 ):
                for i in range(24):
                    displaybuffer[(start - i)]=self.wheel(((start + self.callcounter*3) & 255))
            else:
                for i in range(24):
                    displaybuffer[(start + i)]=self.wheel(((start + self.callcounter*3) & 255))
                



        
#defines for various effects

#s_indianer = Scanner(segment_indianer_halo,(0,255,255))
#ls_indianer.updateinterval=3

#ring0 = Scanner(segment_ring0,(255,0,0,0))
#ring1 = Scanner(segment_ring1,(0,255,0,0))
#ring1.updateinterval=5
#ring1.direction=1


rainbowrings = RainbowRings(segment_ring0,(0,0,0,0))

regenbogensemmel = Regenbogensemmel(segment_ring0,(0,0,0,0))
#regenbogensemmel.updateinterval=1

def run_animations():
    global stepcounter
    stepcounter+=1

    clear()

    #rainbowrings.run()
    regenbogensemmel.run()




def clear():
   for i in range(num_pixels):
        displaybuffer[i]=(0,0,0,0)   









def display_anim_buffer():
    for i in range(num_pixels):
        pixels[i]=displaybuffer[i]
    pixels.write()
#    time.sleep(0.1)






def color_chase(color, wait):
    for i in range(num_pixels):
        pixels[i] = color
        time.sleep(wait)
        pixels.write()
        time.sleep(0.5)






#for i in range(num_pixels):
#    print(i)
#    pixels[i]=(255,0,0)
#    pixels.write()
#    input("Press Enter to continue...")
    

def setup_umbrella():    
    # buffer fuer pixel empty fuellen
    for i in range(num_pixels):
        displaybuffer[i]=(0,0,0,0)    






setup_umbrella()

while True:
    run_animations()
    display_anim_buffer()





# for remote webrepl .. fucked up.
#from flo import *
#def go():
#    ring0 = FullSegment(segment_ring0,(255,0,0,0))
#    run_animations()
#    display_anim_buffer()   
#segment_ring0=(20,24, 71,72, 119,120, 168,179, 217,218,  255,256) #USED
#go()
