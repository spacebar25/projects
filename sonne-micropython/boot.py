import network
import webrepl
import credentials

# Step 1: Connect to WiFi
def wifi_connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(credentials.SSID, credentials.PASSWORD)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())

#wifi_connect()

# Step 2: Start WebREPL
#webrepl.start()
