difference()
{
    cube([53,30,5],center=true);
    translate([0,0,2]) cube([53,5,3],center=true);
    
    translate([20,10,-10]) 
    { 
        cylinder(d=4,h=100,$fn=100);
       translate([0,0,7.5])  cylinder(d1=8,d2=4,h=3,$fn=100);
    }
    
        translate([20,-10,-10]) 
    { 
        cylinder(d=4,h=100,$fn=100);
       translate([0,0,7.5])  cylinder(d1=8,d2=4,h=3,$fn=100);
    }
    
        translate([-20,10,-10]) 
    { 
        cylinder(d=4,h=100,$fn=100);
       translate([0,0,7.5])  cylinder(d1=8,d2=4,h=3,$fn=100);
    }
    

        translate([-20,-10,-10]) 
    { 
        cylinder(d=4,h=100,$fn=100);
       translate([0,0,7.5])  cylinder(d1=8,d2=4,h=3,$fn=100);
    }

}