module inlay()
{
    cylinder(d=18,h=10,$fn=100);
    translate([0,-9,0]) cube([9,18,10]);
    
}




module links()
{
difference()
{
   cube([60,25,15]);
   translate([20,10,5]) rotate([0,0,90]) inlay();
   translate([20,10,-20]) rotate([0,0,90]) cylinder(d=5,h=100,$fn=100);
    
    
   translate([42,10,5]) rotate([0,0,120]) inlay();    
   translate([42,10,-20]) rotate([0,0,90]) cylinder(d=5,h=100,$fn=100);    
    
    
   translate([5,50,7]) rotate([90,0,0]) cylinder(d=5,h=100,$fn=100); 
   translate([5,4,7]) rotate([90,0,0]) cylinder(d1=5,d2=9,h=4,$fn=100); 
    
   translate([55,50,7]) rotate([90,0,0]) cylinder(d=5,h=100,$fn=100); 
   translate([55,4,7]) rotate([90,0,0]) cylinder(d1=5,d2=9,h=4,$fn=100); 

}

}


module rechts()
{
   difference()
{
   cube([60,25,15]);
   translate([20,10,0]) rotate([0,0,90]) inlay();
   translate([42,10,0]) rotate([0,0,120]) inlay();    
   translate([5,50,7]) rotate([90,0,0]) cylinder(d=5,h=100,$fn=100); 
   translate([5,4,7]) rotate([90,0,0]) cylinder(d1=5,d2=9,h=4,$fn=100); 
    
   translate([55,50,7]) rotate([90,0,0]) cylinder(d=5,h=100,$fn=100); 
   translate([55,4,7]) rotate([90,0,0]) cylinder(d1=5,d2=9,h=4,$fn=100); 

}
}



//links();


rechts();